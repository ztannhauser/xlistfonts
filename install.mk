name = $(shell basename $(shell pwd))
install: ~/bin/$(name)

~/bin/$(name): main
	mkdir -p ~/bin
	cp main ~/bin/$(name)

uninstall: name = $(shell basename $(shell pwd))
uninstall:
	rm ~/bin/$(name)
