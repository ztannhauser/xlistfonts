
#include <stdio.h>
#include <X11/Xlib.h>

int main()
{
	Display* display = XOpenDisplay(NULL);
	int n;
	char** names = XListFonts(display, "*", 10000, &n);
	for(int i = 0;i < n;i++)
	{
		printf("\"%s\"\n", names[i]);
	}
	XFreeFontNames(names);
	return 0;
}

